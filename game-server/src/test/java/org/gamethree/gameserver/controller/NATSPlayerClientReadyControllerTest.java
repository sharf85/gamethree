package org.gamethree.gameserver.controller;

import io.nats.client.Connection;
import io.nats.client.Dispatcher;
import io.nats.client.Message;
import io.nats.client.MessageHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NATSPlayerClientReadyControllerTest {

    @Mock
    ApplicationEventPublisher publisher;

    @Mock
    Connection nats;

    @Mock
    Dispatcher dispatcher;

    @Captor
    ArgumentCaptor<MessageHandler> messageHandlerArgumentCaptor;

    NATSPlayerClientReadyController controller;

    @Before
    public void init() {
        when(nats.createDispatcher(messageHandlerArgumentCaptor.capture())).thenReturn(dispatcher);
        controller = spy(new NATSPlayerClientReadyController(nats, "client-ready", publisher));
    }

    @Test
    public void testInit() throws InterruptedException {
        controller.init();

        verify(dispatcher).subscribe("client-ready");
        verify(nats).createDispatcher(messageHandlerArgumentCaptor.capture());
        MessageHandler lambda = messageHandlerArgumentCaptor.getValue();

        Message msg = mock(Message.class);
        when(msg.getData()).thenReturn("player@serverSubject".getBytes());
        lambda.onMessage(msg);
        verify(controller).onPlayerClientIncomingEvent(msg);
    }

    @Captor
    ArgumentCaptor<ClientEventDTO> clientEventDTOArgumentCaptor;

    @Test
    public void testOnPlayerClientIncomingEvent() {
        Message msg = mock(Message.class);
        when(msg.getData()).thenReturn("player@serverSubject".getBytes());
        when(msg.getReplyTo()).thenReturn("clientSubject");

        controller.onPlayerClientIncomingEvent(msg);
        verify(controller).onPlayerClientReady(clientEventDTOArgumentCaptor.capture());

        ClientEventDTO clientEventDTO = clientEventDTOArgumentCaptor.getValue();
        assertEquals(clientEventDTO.serverSubject, "serverSubject");
        assertEquals(clientEventDTO.replyToSubject, "clientSubject");
        assertEquals(clientEventDTO.name, "player");
    }
}
