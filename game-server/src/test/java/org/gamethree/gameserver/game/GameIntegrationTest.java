package org.gamethree.gameserver.game;

import org.gamethree.gameserver.GameServerApplication;
import org.gamethree.gameserver.mock.MockPlayerAgentConnectionAdapter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = GameServerApplication.class,
        initializers = ConfigFileApplicationContextInitializer.class)
@ActiveProfiles("test")
public class GameIntegrationTest {

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    @Qualifier("gameThreadPoolTaskExecutor")
    TaskExecutor taskExecutor;

    @Autowired
    private MockPlayerAgentConnectionAdapter connectionAdapter;

    @Test
    public void test() throws InterruptedException {
        PlayerClientEvent player1 = new PlayerClientEvent(this, "player1", "client1", "client1");
        PlayerClientEvent player2 = new PlayerClientEvent(this, "player2", "client2", "client2");

        Game game = applicationContext.getBean(Game.class);
        game.init(player1, player2);
        taskExecutor.execute(game);

        Thread.sleep(5000);

        //checking first player moves
        List<Integer> history1 = connectionAdapter.getHistories().get(player1.getReplyToSubject());
        int currentMove1 = history1.get(0);

        List<Integer> trueHistory1 = prepareTrueHistory(currentMove1);

        for (int i = 0; i < history1.size(); i++) {
            assertEquals(history1.get(i), trueHistory1.get(i));
        }

        //checking second player moves
        List<Integer> history2 = connectionAdapter.getHistories().get(player2.getReplyToSubject());
        int currentMove2 = history2.get(0);

        List<Integer> trueHistory2 = prepareTrueHistory(currentMove2);

        for (int i = 0; i < history2.size(); i++) {
            assertEquals(history2.get(i), trueHistory2.get(i));
        }

        //checking they are related
        int firstMove1 = history1.get(0);
        int firstMove2 = history2.get(0);
        if (firstMove1 > firstMove2)
            assertEquals(firstMove2, connectionAdapter.makeMove(firstMove1));
        else
            assertEquals(firstMove1, connectionAdapter.makeMove(firstMove2));
    }

    private List<Integer> prepareTrueHistory(int currentMove) {
        List<Integer> trueHistory = new ArrayList<>();
        trueHistory.add(currentMove);
        while (currentMove > 4) {
            int oppMove = connectionAdapter.makeMove(currentMove);
            currentMove = connectionAdapter.makeMove(oppMove);
            trueHistory.add(currentMove);
        }
        return trueHistory;
    }
}
