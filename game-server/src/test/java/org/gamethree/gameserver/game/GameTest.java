package org.gamethree.gameserver.game;

import org.gamethree.gameserver.adapter.MoveSubscriptionInterface;
import org.gamethree.gameserver.adapter.PlayerAgentConnectionAdapterInterface;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {

    private Game game;

    @Mock
    private PlayerAgentConnectionAdapterInterface connection;
    @Mock
    private ThreadPoolTaskScheduler taskScheduler;

    private PlayerClientEvent player1;
    private PlayerClientEvent player2;
    private MoveSubscriptionInterface subscription1;
    private MoveSubscriptionInterface subscription2;

    @Before
    public void init() {
        GameRulesValidator validator = new GameRulesValidator();
        game = spy(new Game(connection, validator, 10000, taskScheduler));

        player1 = new PlayerClientEvent(this, "player1", "client1", "server1");
        player2 = new PlayerClientEvent(this, "player2", "client2", "server2");

        subscription1 = mock(MoveSubscriptionInterface.class);
        subscription2 = mock(MoveSubscriptionInterface.class);

    }

    @Captor
    ArgumentCaptor<PlayerAgentConnectionAdapterInterface.MoveListener> moveListenerArgumentCaptor;

    @Captor
    ArgumentCaptor<Runnable> runnableArgumentCaptor;

    @Test
    public void testRun() {
        when(connection.createSubscription(any(), any())).thenReturn(mock(MoveSubscriptionInterface.class));

        game.init(player1, player2);
        game.run();

        verify(connection).createSubscription(eq("server1"), moveListenerArgumentCaptor.capture());
        PlayerAgentConnectionAdapterInterface.MoveListener lambda1 = moveListenerArgumentCaptor.getValue();

        verify(connection).createSubscription(eq("server2"), moveListenerArgumentCaptor.capture());
        PlayerAgentConnectionAdapterInterface.MoveListener lambda2 = moveListenerArgumentCaptor.getValue();

        lambda1.moveEvent(321);
        verify(game).onPlayer1MoveCome(321);

        lambda2.moveEvent(107);
        verify(game).onPlayer2MoveCome(107);

        verify(taskScheduler, times(2)).schedule(runnableArgumentCaptor.capture(), any(Date.class));
        List<Runnable> lambdas = runnableArgumentCaptor.getAllValues();

        lambdas.get(0).run();
        verify(game).delayedStartGame();

        lambdas.get(1).run();
        verify(game).delayedEndGame();
    }

    @Test
    public void testDelayedStartGame() {
        game.init(player1, player2);

        game.delayedStartGame();
        verify(connection).publishGameStarted(any());
    }

    @Test
    public void testDelayedEndGame() {

        game.init(player1, player2);

//        when(connection.createSubscription(eq("server1"), any())).thenReturn(subscription1);
//        when(connection.createSubscription(eq("server2"), any())).thenReturn(subscription2);

        game.player1Subscription = subscription1;
        game.player2Subscription = subscription2;


        when(subscription1.isActive()).thenReturn(false);
        when(subscription2.isActive()).thenReturn(false);

        game.delayedEndGame();

        verify(subscription1, times(0)).close();
        verify(subscription2, times(0)).close();

        when(subscription1.isActive()).thenReturn(true);
        when(subscription2.isActive()).thenReturn(true);

        game.delayedEndGame();

        verify(subscription1, times(1)).close();
        verify(subscription2, times(1)).close();
    }

    @Test
    public void testOnPlayer1MoveCome_wrongMove() {
        game.player1Subscription = subscription1;
        game.player2Subscription = subscription2;

        game.init(player1, player2);
        game.gameState = spy(game.gameState);

        game.onPlayer1MoveCome(-321);
        verify(game.gameState).setWinner(player2);
        verify(game).endGame();
    }

    @Test
    public void testOnPlayer1MoveCome_rightMove() {
        game.player1Subscription = subscription1;
        game.player2Subscription = subscription2;


        game.init(player1, player2);
        game.gameState = spy(game.gameState);

        game.onPlayer1MoveCome(321);
        verify(connection).publishMove(player2.getReplyToSubject(), 321);

    }

    @Test
    public void testOnPlayer1MoveCome_winningMove() {
        game.player1Subscription = subscription1;
        game.player2Subscription = subscription2;

        game.init(player1, player2);
        game.gameState = spy(game.gameState);
        game.gameState.addPlayer2Move(3);

        game.onPlayer1MoveCome(1);
        verify(game.gameState).setWinner(player1);
        verify(game).endGame();
    }

    @Test
    public void testOnPlayer2MoveCome_wrongMove() {
        game.player1Subscription = subscription1;
        game.player2Subscription = subscription2;

        game.init(player1, player2);
        game.gameState = spy(game.gameState);

        game.onPlayer2MoveCome(-321);
        verify(game.gameState).setWinner(player1);
        verify(game).endGame();
    }

    @Test
    public void testOnPlayer2MoveCome_rightMove() {
        game.player1Subscription = subscription1;
        game.player2Subscription = subscription2;


        game.init(player1, player2);
        game.gameState = spy(game.gameState);

        game.onPlayer2MoveCome(321);
        verify(connection).publishMove(player1.getReplyToSubject(), 321);
    }

    @Test
    public void testOnPlayer2MoveCome_winningMove() {
        game.player1Subscription = subscription1;
        game.player2Subscription = subscription2;

        game.init(player1, player2);
        game.gameState = spy(game.gameState);
        game.gameState.addPlayer1Move(3);

        game.onPlayer2MoveCome(1);
        verify(game.gameState).setWinner(player2);
        verify(game).endGame();
    }

    @Test
    public void testEndGame() {
        game.player1Subscription = subscription1;
        game.player2Subscription = subscription2;

        game.init(player1, player2);
        game.gameState = spy(game.gameState);
        doAnswer(invocation -> {
            System.out.println("game over");
            return null;
        }).when(game.gameState).publish();

        game.endGame();

        verify(connection, times(1)).publishEndGame(player1.getReplyToSubject());
        verify(connection, times(1)).publishEndGame(player2.getReplyToSubject());

        verify(subscription1, times(1)).close();
        verify(subscription2, times(1)).close();

        verify(game.gameState).publish();
    }

    @Test
    public void testGetFirstPlayer() {
        game.init(player1, player2);

        boolean isThere1 = false;
        boolean isThere2 = false;
        for (int i = 0; i < 100; i++) {
            if (game.getFirstPlayer() == player1)
                isThere1 = true;
            else
                isThere2 = true;
        }
        assertEquals(isThere1, isThere2);
    }

    @Test
    public void testIsPlayer1MoveValid() {
        game.init(player1, player2);
        assertTrue(game.gameState.isPlayer1MoveValid(300));
        assertTrue(game.gameState.isPlayer1MoveValid(2));
        assertTrue(game.gameState.isPlayer1MoveValid(3));
        assertFalse(game.gameState.isPlayer1MoveValid(-300));
        assertFalse(game.gameState.isPlayer1MoveValid(1));

        game.gameState.addPlayer2Move(2);
        assertTrue(game.gameState.isPlayer1MoveValid(1));
        game.gameState.player2Moves.clear();

        game.gameState.addPlayer2Move(200);
        assertFalse(game.gameState.isPlayer1MoveValid(1));
        game.gameState.player2Moves.clear();

        game.gameState.addPlayer2Move(200);
        assertTrue(game.gameState.isPlayer1MoveValid(67));
        game.gameState.player2Moves.clear();
    }

    @Test
    public void testIsPlayer2MoveValid() {
        game.init(player1, player2);
        assertTrue(game.gameState.isPlayer2MoveValid(300));
        assertTrue(game.gameState.isPlayer2MoveValid(2));
        assertTrue(game.gameState.isPlayer2MoveValid(3));
        assertFalse(game.gameState.isPlayer2MoveValid(-300));
        assertFalse(game.gameState.isPlayer2MoveValid(1));

        game.gameState.addPlayer1Move(2);
        assertTrue(game.gameState.isPlayer2MoveValid(1));
        game.gameState.player2Moves.clear();

        game.gameState.addPlayer1Move(200);
        assertFalse(game.gameState.isPlayer2MoveValid(1));
        game.gameState.player2Moves.clear();

        game.gameState.addPlayer1Move(200);
        assertTrue(game.gameState.isPlayer2MoveValid(67));
        game.gameState.player2Moves.clear();
    }
}
