package org.gamethree.gameserver.mock;

import org.gamethree.gameserver.adapter.MoveSubscriptionInterface;
import org.gamethree.gameserver.adapter.PlayerAgentConnectionAdapterInterface;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.mockito.Mockito.mock;

/**
 * The class is for integration tests. The only thing which can be received from the clients by business logic is a move.
 */
public class MockPlayerAgentConnectionAdapter implements PlayerAgentConnectionAdapterInterface {

    private Map<String, MoveListener> moveListeners = new HashMap<>();
    Map<String, List<Integer>> histories = new HashMap<>();

    public MockPlayerAgentConnectionAdapter() {

    }

    private BlockingQueue<Node> messagesQueue = new LinkedBlockingQueue<>();

    public void init() {
        new Thread(this::handleQueue).start();
    }

    private void handleQueue() {
        while (true) {
            try {
                Node node = messagesQueue.take();
                String message = node.message;
                new Thread(() -> moveListeners.get(node.subscription).moveEvent(Integer.parseInt(message))).start();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void publishMove(String subjectName, int move) {
        int moveOut = makeMove(move);
        histories.get(subjectName).add(moveOut);
        messagesQueue.offer(new Node(subjectName, Integer.toString(moveOut)));
    }

    public int makeMove(int input) {
        int output = input / 3;
        int remainder = input % 3;

        if (remainder == 2)
            output++;

        return output;
    }

    private int makeFirstMove() {
        return new Random().nextInt(Integer.MAX_VALUE - 2) + 2;
    }

    @Override
    public void publishEndGame(String subjectName) {
        //nothing to do in mock
    }

    @Override
    public void publishGameStarted(String subjectName) {
        int moveOut = makeFirstMove();
        histories.get(subjectName).add(moveOut);
        messagesQueue.offer(new Node(subjectName, Integer.toString(moveOut)));
    }

    @Override
    public MoveSubscriptionInterface createSubscription(String subjectName, MoveListener moveListener) {
        moveListeners.put(subjectName, moveListener);
        histories.put(subjectName, new ArrayList<>());
        return mock(MoveSubscriptionInterface.class);
    }

    private static class Node {
        Node(String subscription, String message) {
            this.subscription = subscription;
            this.message = message;
        }

        String subscription;
        String message;
    }

    public Map<String, List<Integer>> getHistories() {
        return histories;
    }
}