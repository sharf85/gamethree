package org.gamethree.gameserver.config;

import org.gamethree.gameserver.mock.MockPlayerAgentConnectionAdapter;
import org.springframework.context.annotation.*;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@Profile("test")
@EnableAsync
public class TestConfig {

    @Bean
    public MockPlayerAgentConnectionAdapter mockPlayerAgentConnectionAdapter() {
        MockPlayerAgentConnectionAdapter res = new MockPlayerAgentConnectionAdapter();
        res.init();

        return res;
    }
}
