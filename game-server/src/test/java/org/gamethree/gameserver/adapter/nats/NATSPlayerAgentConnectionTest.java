package org.gamethree.gameserver.adapter.nats;

import io.nats.client.Connection;
import io.nats.client.Dispatcher;
import io.nats.client.Message;
import io.nats.client.MessageHandler;
import org.gamethree.gameserver.adapter.PlayerAgentConnectionAdapterInterface;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NATSPlayerAgentConnectionTest {
    private NATSPlayerAgentConnection adapter;
    private Connection nats;

    @Before
    public void init() {
        nats = mock(Connection.class);
        adapter = new NATSPlayerAgentConnection("end", "start", nats);
        adapter = spy(adapter);
    }

    @Test
    public void testPublishEndGame() {
        adapter.publishEndGame("replyToSubject");
        verify(nats, times(1)).publish("replyToSubject", "end".getBytes());
    }

    @Test
    public void testGameStarted() {
        adapter.publishGameStarted("replyToSubject");
        verify(nats, times(1)).publish("replyToSubject", "start".getBytes());
    }

    @Captor
    private ArgumentCaptor<MessageHandler> registerMessageLambdaCaptor;

    @Test
    public void testCreateSubscription() throws InterruptedException {
        PlayerAgentConnectionAdapterInterface.MoveListener moveListener = mock(PlayerAgentConnectionAdapterInterface.MoveListener.class);
        Dispatcher dispatcher = mock(Dispatcher.class);
        when(nats.createDispatcher(any(MessageHandler.class))).thenReturn(dispatcher);


        adapter.createSubscription("subjectName", moveListener);
        verify(dispatcher).subscribe("subjectName");

        verify(nats).createDispatcher(registerMessageLambdaCaptor.capture());
        MessageHandler lambda = registerMessageLambdaCaptor.getValue();


        Message msg = mock(Message.class);
        when(msg.getData()).thenReturn("10000".getBytes());
        lambda.onMessage(msg);
        verify(adapter, times(1)).onMoveCame(msg, moveListener);
    }


    @Test
    public void testOnMoveCame() {
        PlayerAgentConnectionAdapterInterface.MoveListener moveListener = mock(PlayerAgentConnectionAdapterInterface.MoveListener.class);
        Message msg = mock(Message.class);

        //test start of the game signal
        when(msg.getData()).thenReturn("1000".getBytes());
        adapter.onMoveCame(msg, moveListener);

        verify(moveListener, times(1)).moveEvent(1000);
    }
}
