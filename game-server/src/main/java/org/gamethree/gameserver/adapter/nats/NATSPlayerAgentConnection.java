package org.gamethree.gameserver.adapter.nats;

import io.nats.client.Connection;
import io.nats.client.Dispatcher;
import io.nats.client.Message;
import org.gamethree.gameserver.adapter.MoveSubscriptionInterface;
import org.gamethree.gameserver.adapter.PlayerAgentConnectionAdapterInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * An implementation via Nats messenger. It should not be instantiated while testing.
 */
@Service
@Profile("!test")
public class NATSPlayerAgentConnection implements PlayerAgentConnectionAdapterInterface {

    // The two fields are taken from the props
    private final String endGameSignal;
    private final String startGameSignal;

    private final Connection nats;

    @Autowired
    public NATSPlayerAgentConnection(@Value("${gamethree.nats.connection.end_game_signal}") String endGameSignal,
                                     @Value("${gamethree.nats.connection.start_game_signal}") String startGameSignal,
                                     Connection nats) {
        this.endGameSignal = endGameSignal;
        this.startGameSignal = startGameSignal;
        this.nats = nats;

    }

    @Override
    public MoveSubscriptionInterface createSubscription(String subjectName,
                                                        MoveListener moveListener) {

        Dispatcher d = nats.createDispatcher(msg -> onMoveCame(msg, moveListener));

        d.subscribe(subjectName);
        return new NATSMoveSubscription(d, nats);
    }

    void onMoveCame(Message msg, MoveListener moveListener) {
        String message = new String(msg.getData());
        moveListener.moveEvent(Integer.parseInt(message));
    }


    @Override
    public void publishMove(String subjectName, int move) {
        nats.publish(subjectName, Integer.toString(move).getBytes());
    }

    @Override
    public void publishEndGame(String subjectName) {
        nats.publish(subjectName, endGameSignal.getBytes());
    }

    @Override
    public void publishGameStarted(String subjectName) {
        nats.publish(subjectName, startGameSignal.getBytes());
    }

}
