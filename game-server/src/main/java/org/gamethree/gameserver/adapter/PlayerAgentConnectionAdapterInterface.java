package org.gamethree.gameserver.adapter;

/**
 * Instances of the class perform all the communication to the player client. The interface is needed to be able
 * to implement the functionality via either of message brokers, http or other staff.
 */
public interface PlayerAgentConnectionAdapterInterface {


    /**
     * The method is listening for the moves of a player. The business logic of the interaction between the
     * server and the Player Client assumes that only a number can come
     *
     * @param subjectName  - the name of the subject to listen to
     * @param moveListener - the function preforming actions when move comes
     */
    MoveSubscriptionInterface createSubscription(String subjectName, MoveListener moveListener);

    /**
     * The method publishes a move of one player to another
     *
     * @param subjectName - the topic where the message sends
     * @param move        - integer value
     */
    void publishMove(String subjectName, int move);

    /**
     * The method publishes the game is over
     *
     * @param subjectName - the topic where the message sends
     */
    void publishEndGame(String subjectName);

    /**
     * signals that the game started to one of the ready Clients, then the Client makes the first turn
     */
    void publishGameStarted(String subjectName);

    /**
     * Service class corresponding to the lambdas accepted by createSubscription method for successful handling
     * incoming moves from the Player Clients
     */
    interface MoveListener {

        /**
         * The event is triggered when the player sends his move
         *
         * @param move - the move of the player
         */
        void moveEvent(int move);
    }

}
