package org.gamethree.gameserver.adapter;

/**
 * Instances of the subscriptions, accepted while {@link PlayerAgentConnectionAdapterInterface#createSubscription(String, PlayerAgentConnectionAdapterInterface.MoveListener)
 * PlayerAgentConnectionAdapterInterface#createSubscription}. Needed for closing connections, unsubscribe etc.
 */

public interface MoveSubscriptionInterface {

    boolean isActive();

    void close();
}
