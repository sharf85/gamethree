package org.gamethree.gameserver.controller;

import io.nats.client.Connection;
import io.nats.client.Dispatcher;
import io.nats.client.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;

/**
 * Nats implementation. Listens to a special subject.
 */
public class NATSPlayerClientReadyController extends AbstractPlayerClientReadyController {

    private final String natsReadyClientsTopic;
    private final Connection natsConnection;

    @Autowired
    public NATSPlayerClientReadyController(Connection natsConnection,
                                           @Value("${nats.ready_player_clients_topic}") String natsReadyClientsTopic,
                                           ApplicationEventPublisher applicationEventPublisher) {
        super(applicationEventPublisher);
        this.natsReadyClientsTopic = natsReadyClientsTopic;
        this.natsConnection = natsConnection;

    }

    public void init() {
        Dispatcher d = natsConnection.createDispatcher(this::onPlayerClientIncomingEvent);
        d.subscribe(natsReadyClientsTopic);
    }

    void onPlayerClientIncomingEvent(Message message) {
        String replyToSubject = message.getReplyTo();
        String data = new String(message.getData());

        String[] splittedData = data.split("@");
        String playerName = splittedData[0];
        String serverSubject = splittedData[1];

        onPlayerClientReady(new ClientEventDTO(playerName, replyToSubject, serverSubject));
    }

}

