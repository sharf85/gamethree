package org.gamethree.gameserver.controller;

import org.gamethree.gameserver.game.PlayerClientEvent;
import org.springframework.context.ApplicationEventPublisher;

/**
 * Implementations of the class should listening for the Player Clients which are ready to play. Once a Player is caught,
 * it passes further, to GameManager, which stacks it and starts new game between two stacked players, if present
 */
public abstract class AbstractPlayerClientReadyController {

    protected final ApplicationEventPublisher applicationEventPublisher;

    protected AbstractPlayerClientReadyController(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void onPlayerClientReady(ClientEventDTO eventIn) {
        applicationEventPublisher.publishEvent(new PlayerClientEvent(this, eventIn.name, eventIn.replyToSubject, eventIn.serverSubject));
    }

}

class ClientEventDTO {
    String serverSubject;
    String replyToSubject;
    String name;

    public ClientEventDTO(String name, String replyToSubject, String serverSubject) {
        this.name = name;
        this.replyToSubject = replyToSubject;
        this.serverSubject = serverSubject;
    }
}
