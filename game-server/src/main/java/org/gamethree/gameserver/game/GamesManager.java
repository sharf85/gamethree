package org.gamethree.gameserver.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * The only purpose of the component is to accept players and start games
 */
@Component
@Profile("!test")
public class GamesManager implements CommandLineRunner {

    private final TaskExecutor taskExecutor;

    private final ApplicationContext applicationContext;

    /**
     * This is a queue of the incommi4ng players. It is concurrent and blocking, which allows to start game between
     * the players come before others, and freezes in case of none of the players are present.
     */
    private BlockingQueue<PlayerClientEvent> playersToStartQueue;

    @Autowired
    public GamesManager(@Qualifier("gameThreadPoolTaskExecutor") TaskExecutor taskExecutor,
                        ApplicationContext applicationContext) {
        this.taskExecutor = taskExecutor;
        this.applicationContext = applicationContext;

        playersToStartQueue = new LinkedBlockingQueue<>();
    }

    @EventListener()
    public void onNewPlayerClient(PlayerClientEvent eventIn) {
        playersToStartQueue.offer(eventIn);
    }

    /**
     * The main thread of the server, never stops. Starts games in separate threads
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        while (true) {
            taskExecutor.execute(prepareNewGame());
        }
    }

    /**
     * Initialises new games with two closest players
     * @return
     * @throws InterruptedException
     */
    protected Game prepareNewGame() throws InterruptedException {
        PlayerClientEvent player1Event = playersToStartQueue.take();
        PlayerClientEvent player2Event = playersToStartQueue.take();
        Game newGame = applicationContext.getBean(Game.class);
        newGame.init(player1Event, player2Event);

        return newGame;
    }

}
