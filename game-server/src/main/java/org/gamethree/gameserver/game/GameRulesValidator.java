package org.gamethree.gameserver.game;

import org.springframework.stereotype.Service;

/**
 * The class is intended for validating all the moves made
 */
@Service
public class GameRulesValidator {

    /**
     * @param move - the first move of a game
     * @return whether the opponent's move is correct
     */
    public boolean validateSingleMove(int move) {
        return move >= 2;
    }

    /**
     * @param inMove  - the move of the opponent
     * @param outMove - the move of the player based on the opponent's move
     * @return whether the player's move is correct regarding to the opponent's move
     */
    public boolean validateNextMove(int inMove, int outMove) {
        if (!validateSingleMove(inMove))
            return false;

        int reminder = inMove % 3;
        return reminder == 2 && outMove * 3 == inMove + 1 || outMove * 3 == inMove - reminder;
    }

    /**
     * @param move - a move
     * @return whether the move is a winning one
     */
    public boolean isWinningMove(int move) {
        return move == 1;
    }
}
