package org.gamethree.gameserver.game;

import org.gamethree.gameserver.adapter.MoveSubscriptionInterface;
import org.gamethree.gameserver.adapter.PlayerAgentConnectionAdapterInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.io.BufferedOutputStream;
import java.io.PrintWriter;
import java.util.*;

/**
 * The class for a game flow. When there are enough players in {@link GamesManager}, an instance is created. It is
 * marked as prototype since every game is a single thread
 */
@Component
@Scope("prototype")
public class Game implements Runnable {

    /**
     * The duration, after which the game closes automatically
     */
    @Value("${gamethree.game.duration}")
    private long gameLength;

    /**
     * Communication instance with Player Client
     */
    private final PlayerAgentConnectionAdapterInterface connectionAdapter;
    /**
     * Validates received form players moves
     */
    private final GameRulesValidator rulesValidator;
    private final ThreadPoolTaskScheduler taskScheduler;

    @Autowired
    public Game(PlayerAgentConnectionAdapterInterface connectionAdapter,
                GameRulesValidator rulesValidator,
                @Value("${gamethree.game.duration}") long gameLength,
                ThreadPoolTaskScheduler taskScheduler) {
        this.connectionAdapter = connectionAdapter;
        this.rulesValidator = rulesValidator;
        this.gameLength = gameLength;
        this.taskScheduler = taskScheduler;
    }

    //These two vars are initialized after the instance, as they are not autowired. The data of the player clients
    private PlayerClientEvent player1Event;
    private PlayerClientEvent player2Event;

    MoveSubscriptionInterface player1Subscription;
    MoveSubscriptionInterface player2Subscription;

    /**
     * The state of the game. Keeps all the moves done, who is the winner etc.
     */
    GameState gameState;

    void init(PlayerClientEvent player1Event, PlayerClientEvent player2Event) {
        this.player1Event = player1Event;
        this.player2Event = player2Event;

        this.gameState = new GameState();
    }

    @Override
    public void run() {
        player1Subscription = connectionAdapter.createSubscription(player1Event.getServerSubject(), this::onPlayer1MoveCome);
        player2Subscription = connectionAdapter.createSubscription(player2Event.getServerSubject(), this::onPlayer2MoveCome);

        //to make sure that the subscriptions are up
        Date startDate = new Date(System.currentTimeMillis() + 2000);
        taskScheduler.schedule(this::delayedStartGame, startDate);

        // shut downt the game if too much time passed
        Date endDate = new Date(System.currentTimeMillis() + gameLength + 2000);
        taskScheduler.schedule(this::delayedEndGame, endDate);
    }

    void delayedStartGame() {
        PlayerClientEvent firstPlayer = getFirstPlayer();
        connectionAdapter.publishGameStarted(firstPlayer.getReplyToSubject());
    }

    /**
     * shuts down the game in a single delayed thread
     */
    void delayedEndGame() {
        if (player1Subscription.isActive())
            player1Subscription.close();
        if (player2Subscription.isActive())
            player2Subscription.close();
    }

    /**
     * handles the moves come from the first player, validates and sends the move to the second player
     *
     * @param move - incoming player1's move
     */
    void onPlayer1MoveCome(int move) {
        gameState.addPlayer1Move(move);
        if (!gameState.isPlayer1MoveValid(move)) {
            gameState.setWinner(player2Event);
            endGame();
            return;
        }

        if (rulesValidator.isWinningMove(move)) {
            gameState.setWinner(player1Event);
            endGame();
            return;
        }

        connectionAdapter.publishMove(player2Event.getReplyToSubject(), move);
    }

    /**
     * the same method as previous, but vice versa
     *
     * @param move - incoming player2's move
     */
    void onPlayer2MoveCome(int move) {
        gameState.addPlayer2Move(move);
        if (!gameState.isPlayer2MoveValid(move)) {
            gameState.setWinner(player1Event);
            endGame();
            return;
        }

        if (rulesValidator.isWinningMove(move)) {
            gameState.setWinner(player2Event);
            endGame();
            return;
        }

        connectionAdapter.publishMove(player1Event.getReplyToSubject(), move);
    }


    /**
     * ending the game if everything is played normally
     */
    void endGame() {
        connectionAdapter.publishEndGame(player1Event.getReplyToSubject());
        connectionAdapter.publishEndGame(player2Event.getReplyToSubject());

        player1Subscription.close();
        player2Subscription.close();

        gameState.publish();
    }

    /**
     * @return random player who makes first move
     */
    PlayerClientEvent getFirstPlayer() {
        return new Random().nextInt(2) == 0 ? player1Event : player2Event;
    }

    /**
     * The data of the running game
     */
    protected class GameState {
        LinkedList<Integer> player1Moves;
        LinkedList<Integer> player2Moves;
        PlayerClientEvent winner;

        GameState() {
            player1Moves = new LinkedList<>();
            player2Moves = new LinkedList<>();
        }

        void addPlayer1Move(int move) {
            this.player1Moves.add(move);
        }

        void addPlayer2Move(int move) {
            this.player2Moves.add(move);
        }

        /**
         * Validates if the move of the player1 is correct according to the game state
         *
         * @param move - incoming player1 move
         * @return correct or not
         */
        boolean isPlayer1MoveValid(int move) {
            return player2Moves.size() == 0 && rulesValidator.validateSingleMove(move) || player2Moves.size() > 0 && rulesValidator.validateNextMove(player2Moves.getLast(), move);
        }

        /**
         * Validates if the move of the player2 is correct according to the game state
         *
         * @param move - incoming player2 move
         * @return correct or not
         */
        boolean isPlayer2MoveValid(int move) {
            return player1Moves.size() == 0 && rulesValidator.validateSingleMove(move) || player1Moves.size() > 0 && rulesValidator.validateNextMove(player1Moves.getLast(), move);
        }

        void setWinner(PlayerClientEvent winner) {
            this.winner = winner;
        }

        /**
         * This is a method that should be invoked after the game is over. Here is just an example, the right way should be
         * a special publishing service as an autowired bean of the Game class to pass the GameState instance further in.
         */
        void publish() {

            PrintWriter writer = new PrintWriter(new BufferedOutputStream(System.out));

            writer.println("The game between " + player1Event.getPlayerName() + " and " + player2Event.getPlayerName());

            writer.println(player1Event.getPlayerName() + " moves:");
            player1Moves.forEach(writer::println);
            writer.println(player2Event.getPlayerName() + " moves:");
            player2Moves.forEach(writer::println);

            writer.println(winner.getPlayerName() + " won!");
            writer.flush();
        }

    }

}

