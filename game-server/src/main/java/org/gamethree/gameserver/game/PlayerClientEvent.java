package org.gamethree.gameserver.game;

import org.springframework.context.ApplicationEvent;

/**
 * A POJO class, reflecting data of a player, used in {@link Game} as well as while passing data from
 * {@link org.gamethree.gameserver.controller.AbstractPlayerClientReadyController} to {@link GamesManager} via
 * {@link org.springframework.context.ApplicationEventPublisher}
 */
public class PlayerClientEvent extends ApplicationEvent {
    private String playerName;
    private String replyToSubject;
    private String serverSubject;

    public PlayerClientEvent(Object source, String playerName, String replyToSubject, String serverSubject) {
        super(source);
        this.playerName = playerName;
        this.replyToSubject = replyToSubject;
        this.serverSubject = serverSubject;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getReplyToSubject() {
        return replyToSubject;
    }

    public String getServerSubject() {
        return serverSubject;
    }
}
