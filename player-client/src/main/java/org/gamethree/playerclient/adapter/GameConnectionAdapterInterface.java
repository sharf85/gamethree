package org.gamethree.playerclient.adapter;

/**
 * Instances of the class perform all the communication to the game server. The interface is needed to be able
 * to implement the functionality via either of message brokers, http or other staff.
 */
public interface GameConnectionAdapterInterface {

    /**
     * The method publishes that the Player Client is ready for the game
     *
     * @param serverSubject  - the subject server should be listening to for communication with the client
     * @param replyToSubject - the subject for calling to the client from the server
     */
    void publishClientReady(String serverSubject, String replyToSubject);

    /**
     * The method publishes the player's move
     *
     * @param subjectName - the topic where the message sends to
     * @param move        - integer value
     */
    void publishMove(String subjectName, int move);


    /**
     * The method is listening for the moves of the opponent from the game server.
     *
     * @param subjectName  - the name of the subject to listen to
     * @param moveListener - the function preforming actions when move comes
     */
    MoveSubscriptionInterface createSubscription(String subjectName,
                                                 MoveListener moveListener,
                                                 ExceptionListener exceptionListener);

    /**
     * Close the connection, stop interact with the message server
     */
    void close();

    /**
     * Service class corresponding to the lambdas accepted by createSubscription method for successful handling
     * incoming moves from the server
     */
    interface MoveListener {

        /**
         * The event is triggered when the player sends his move
         *
         * @param move - the move of the opponent from the server
         */
        void moveEvent(int move);
    }

    /**
     * Service class corresponding to the lambdas accepted by createSubscription method for exceptions handling
     * incoming moves from the server. For example, if the start game or end game signals are coming, exceptionEvent is
     * triggering
     */
    interface ExceptionListener {
        /**
         * @param e - exception while execution of createSubscription method. It can be either {@link StartGameException}
         *          or {@link EndGameException}
         */
        void exceptionEvent(Exception e);
    }

}
