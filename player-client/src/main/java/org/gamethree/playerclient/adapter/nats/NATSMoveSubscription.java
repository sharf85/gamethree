package org.gamethree.playerclient.adapter.nats;

import io.nats.client.Connection;
import io.nats.client.Dispatcher;
import org.gamethree.playerclient.adapter.MoveSubscriptionInterface;

public class NATSMoveSubscription implements MoveSubscriptionInterface {

    private Dispatcher dispatcher;
    private Connection nats;

    public NATSMoveSubscription(Dispatcher dispatcher, Connection nats) {
        this.dispatcher = dispatcher;
        this.nats = nats;
    }

    @Override
    public boolean isActive() {
        return dispatcher.isActive();
    }

    @Override
    public void close() {
        nats.closeDispatcher(dispatcher);
    }
}
