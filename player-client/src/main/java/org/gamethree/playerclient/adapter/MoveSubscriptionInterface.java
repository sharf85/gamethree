package org.gamethree.playerclient.adapter;

/**
 * Instances of the subscriptions, accepted while {@link GameConnectionAdapterInterface#createSubscription(String,
 * GameConnectionAdapterInterface.MoveListener, GameConnectionAdapterInterface.ExceptionListener)
 * GameConnectionAdapterInterface#createSubscription}. Needed for closing connections, unsubscribe etc.
 */
public interface MoveSubscriptionInterface {

    boolean isActive();

    void close();
}
