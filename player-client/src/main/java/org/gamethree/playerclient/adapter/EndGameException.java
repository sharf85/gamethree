package org.gamethree.playerclient.adapter;

/**
 * a service class to signalize the game is over
 */
public class EndGameException extends Exception {
}
