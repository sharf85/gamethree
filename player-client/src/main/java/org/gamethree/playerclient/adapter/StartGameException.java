package org.gamethree.playerclient.adapter;

/**
 * A service class to signalize the game got started
 */
public class StartGameException extends Exception {
}
