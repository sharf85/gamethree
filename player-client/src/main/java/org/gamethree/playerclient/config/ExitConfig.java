package org.gamethree.playerclient.config;

import org.gamethree.playerclient.client.ExitServiceInteface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!test")
public class ExitConfig {

    @Autowired
    private ApplicationContext applicationContext;

    @Bean
    public ExitServiceInteface exitService() {
        return () -> SpringApplication.exit(applicationContext, () -> 0);
    }
}
