package org.gamethree.playerclient.config;

import io.nats.client.Connection;
import io.nats.client.Nats;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.io.IOException;

@Configuration
@Profile("!test")
public class NatsConfig {

    @Bean
    public Connection natsConnection() throws IOException, InterruptedException {
        return Nats.connect();
    }

}
