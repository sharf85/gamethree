package org.gamethree.playerclient.client;

import org.gamethree.playerclient.adapter.EndGameException;
import org.gamethree.playerclient.adapter.GameConnectionAdapterInterface;
import org.gamethree.playerclient.adapter.MoveSubscriptionInterface;
import org.gamethree.playerclient.adapter.StartGameException;
import org.gamethree.playerclient.agent.PlayerAgentInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

/**
 * The class encapsulates the most of the business logic of the game client side
 */
@Component
@Profile("!test")
public class GameFlow implements CommandLineRunner {

    private final ExitServiceInteface exitService;
    private final GameConnectionAdapterInterface connection;
    private final PlayerAgentInterface playerAgent;
    private final long gameLength;
    private final ThreadPoolTaskScheduler cancelFlowScheduler;

    private final String clientSubject;
    private String serverSubject;

    @Autowired
    public GameFlow(GameConnectionAdapterInterface connection,
                    PlayerAgentInterface playerAgentInterface,
                    @Value("${gamethree.game.duration}") long gameLength,
                    ThreadPoolTaskScheduler cancelFlowScheduler,
                    ExitServiceInteface exitService) {
        this.connection = connection;
        this.playerAgent = playerAgentInterface;
        this.gameLength = gameLength;
        this.cancelFlowScheduler = cancelFlowScheduler;
        this.exitService = exitService;

        this.serverSubject = UUID.randomUUID().toString();
        this.clientSubject = UUID.randomUUID().toString();
    }

    private MoveSubscriptionInterface subscription;

    /**
     * The main process, creates subscription for the moves incoming from the server, publishes that the client is
     * ready for the game and closes the client in some time forcibly (if the game never started)
     * @param args - unused here, incoming params
     */
    @Override
    public void run(String... args) {
        subscription = connection.createSubscription(clientSubject, this::onMoveCome, this::onExceptionCome);

        connection.publishClientReady(serverSubject, clientSubject);

        Date endDate = new Date(System.currentTimeMillis() + gameLength);
        cancelFlowScheduler.schedule(this::endFlow, endDate);
    }

    /**
     * closes everything, shuts down the game
     */
    void endFlow() {
        subscription.close();
        exitService.exit();
    }

    /**
     * handler for incoming exceptions, if server sends something except moves.
     * @param e - a kind of the exception come. The only two kinds are possible here.
     */
    void onExceptionCome(Exception e) {
        if (e instanceof StartGameException) {
            int moveOut = playerAgent.makeFirstMove();
            connection.publishMove(serverSubject, moveOut);
        } else if (e instanceof EndGameException) {
            endFlow();
        }
    }

    /**
     * handler for incoming opponent's moves
     * @param move - teh move of the opponent
     */
    void onMoveCome(int move) {
        int moveOut = playerAgent.makeMove(move);
        connection.publishMove(serverSubject, moveOut);
    }

}
