package org.gamethree.playerclient.agent;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * A bot implementation. Automatically makes correct moves without any validation (a business logic of the server)
 */
@Service
public class PlayerAgent implements PlayerAgentInterface {


    private final String name;

    public PlayerAgent(@Value("${gamethree.agent.name}") String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int makeFirstMove() {
        return new Random().nextInt(Integer.MAX_VALUE - 2) + 2;
    }

    @Override
    public int makeMove(int input) {
        int output = input / 3;
        int remainder = input % 3;

        if (remainder == 2)
            output++;

        return output;
    }

}
