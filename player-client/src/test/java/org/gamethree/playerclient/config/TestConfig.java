package org.gamethree.playerclient.config;

import org.gamethree.playerclient.client.ExitServiceInteface;
import org.gamethree.playerclient.mock.MockGameConnectionAdapter;
import org.springframework.context.annotation.*;

/**
 * Beans for testing
 */
@Configuration
@Profile("test")
public class TestConfig {

    @Bean
    public MockGameConnectionAdapter mockGameConnectionAdapter_isFirstPlayer() {
        MockGameConnectionAdapter adapter = new MockGameConnectionAdapter(true);
        adapter.init();
        return adapter;
    }

    @Bean
    public MockGameConnectionAdapter mockGameConnectionAdapter_isSecondPlayer() {
        MockGameConnectionAdapter adapter = new MockGameConnectionAdapter(false);
        adapter.init();
        return adapter;
    }

    @Bean
    public ExitServiceInteface mockExitService() {
        return () -> {
        };
    }
}
