package org.gamethree.playerclient.mock;

import org.gamethree.playerclient.adapter.EndGameException;
import org.gamethree.playerclient.adapter.GameConnectionAdapterInterface;
import org.gamethree.playerclient.adapter.MoveSubscriptionInterface;
import org.gamethree.playerclient.adapter.StartGameException;
import org.gamethree.playerclient.agent.PlayerAgentInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.mockito.Mockito.mock;

/**
 * The class is for integration tests. When the Client tries to call to the server to inform that it's ready to begin,
 * the mocking connection sends the signal, that the current Player Client is first, or immediately sends the first move
 * of the opponent - depending on the constructor
 */
public class MockGameConnectionAdapter implements GameConnectionAdapterInterface {

    private final boolean isFirst;

    public MockGameConnectionAdapter(boolean isFirst) {
        this.isFirst = isFirst;
    }

    private BlockingQueue<Node> messagesQueue = new LinkedBlockingQueue<>();

    @Autowired
    PlayerAgentInterface playerAgent;

    @Value("${gamethree.nats.connection.start_game_signal}")
    private String startGameSignal;
    @Value("${gamethree.nats.connection.end_game_signal}")
    private String endGameSignal;


    public void init() {
        new Thread(this::handleQueue).start();
    }

    private void handleQueue() {
        while (true) {
            try {
                Node node = messagesQueue.take();
                String message = node.message;

                if (message.equals(startGameSignal)) {
                    new Thread(() -> exceptionListeners.get(node.subscription).exceptionEvent(new StartGameException())).start();
                    continue;
                }

                if (message.equals(endGameSignal)) {
                    new Thread(() -> exceptionListeners.get(node.subscription).exceptionEvent(new EndGameException())).start();
                    continue;
                }

                new Thread(() -> moveListeners.get(node.subscription).moveEvent(Integer.parseInt(message))).start();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void publishClientReady(String serverSubject, String replyToSubject) {
        if (isFirst)
            this.messagesQueue.add(new Node(replyToSubject, startGameSignal));
        else
            this.messagesQueue.add(new Node(replyToSubject, Integer.toString(playerAgent.makeFirstMove())));
    }

    @Override
    public void publishMove(String subjectName, int move) {
        movesHistory.add(move);

        int moveOut = playerAgent.makeMove(move);

        if (move == 1 || moveOut == 1)
            messagesQueue.add(new Node(subjectName, endGameSignal));
        else
            messagesQueue.add(new Node(subjectName, Integer.toString(moveOut)));

    }

    private Map<String, MoveListener> moveListeners = new HashMap<>();
    private Map<String, ExceptionListener> exceptionListeners = new HashMap<>();

    @Override
    public MoveSubscriptionInterface createSubscription(String subjectName, MoveListener moveListener, ExceptionListener exceptionListener) {
        moveListeners.put(subjectName, moveListener);
        exceptionListeners.put(subjectName, exceptionListener);
        return mock(MoveSubscriptionInterface.class);
    }

    @Override
    public void close() {

    }

    private List<Integer> movesHistory = new ArrayList<>();

    public List<Integer> getMovesHistory() {
        return movesHistory;
    }

    private static class Node {
        Node(String subscription, String message) {
            this.subscription = subscription;
            this.message = message;
        }

        String subscription;
        String message;
    }
}