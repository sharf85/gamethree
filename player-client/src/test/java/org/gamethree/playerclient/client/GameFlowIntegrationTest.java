package org.gamethree.playerclient.client;

import org.gamethree.playerclient.PlayerClientApplication;
import org.gamethree.playerclient.agent.PlayerAgentInterface;
import org.gamethree.playerclient.mock.MockGameConnectionAdapter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PlayerClientApplication.class,
        initializers = ConfigFileApplicationContextInitializer.class)
@ActiveProfiles("test")
public class GameFlowIntegrationTest {

    @Qualifier("mockGameConnectionAdapter_isFirstPlayer")
    @Autowired
    private MockGameConnectionAdapter connectionFirst;

    @Qualifier("mockGameConnectionAdapter_isSecondPlayer")
    @Autowired
    private MockGameConnectionAdapter connectionSecond;

    @Autowired
    private PlayerAgentInterface playerAgent;

    private final long gameLength = 10000;

    @Autowired
    private ThreadPoolTaskScheduler cancelFlowScheduler;

    @Mock
    private ExitServiceInteface exitService;

    private GameFlow gameFlow;


    @Test
    public void testGame_playerMovesFirst() throws Exception {
        gameFlow = new GameFlow(connectionFirst, playerAgent, gameLength, cancelFlowScheduler, exitService);
        // this is done for mocking connection object
        String clientSubject = (String) ReflectionTestUtils.getField(gameFlow, "clientSubject");
        ReflectionTestUtils.setField(gameFlow, "serverSubject", clientSubject);

        //run the game
        gameFlow.run();
        //sleep since the flow is asynchronous
        Thread.sleep(1000);

        List<Integer> history = connectionFirst.getMovesHistory();
        int currentMove = history.get(0);

        List<Integer> trueHistory = prepareTrueHistory(currentMove);

        for (int i = 0; i < history.size(); i++) {
            assertEquals(history.get(i), trueHistory.get(i));
        }
    }

    @Test
    public void testGame_playerMovesSecond() throws Exception {
        gameFlow = new GameFlow(connectionSecond, playerAgent, gameLength, cancelFlowScheduler, exitService);
        // this is done for mocking connection object
        String clientSubject = (String) ReflectionTestUtils.getField(gameFlow, "clientSubject");
        ReflectionTestUtils.setField(gameFlow, "serverSubject", clientSubject);

        //run the game
        gameFlow.run();
        //sleep since the flow is asynchronous
        Thread.sleep(1000);

        List<Integer> history = connectionSecond.getMovesHistory();
        assertTrue(history.size() > 0);
        int currentMove = history.get(0);

        List<Integer> trueHistory = prepareTrueHistory(currentMove);

        for (int i = 0; i < history.size(); i++) {
            assertEquals(history.get(i), trueHistory.get(i));
        }
    }


    private List<Integer> prepareTrueHistory(int currentMove) {
        List<Integer> trueHistory = new ArrayList<>();
        trueHistory.add(currentMove);
        while (currentMove > 4) {
            int oppMove = playerAgent.makeMove(currentMove);
            currentMove = playerAgent.makeMove(oppMove);
            trueHistory.add(currentMove);
        }
        return trueHistory;
    }

}
