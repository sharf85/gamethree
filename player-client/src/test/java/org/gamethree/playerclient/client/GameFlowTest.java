package org.gamethree.playerclient.client;

import org.gamethree.playerclient.adapter.EndGameException;
import org.gamethree.playerclient.adapter.GameConnectionAdapterInterface;
import org.gamethree.playerclient.adapter.MoveSubscriptionInterface;
import org.gamethree.playerclient.adapter.StartGameException;
import org.gamethree.playerclient.agent.PlayerAgentInterface;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class GameFlowTest {

    @Mock
    private GameConnectionAdapterInterface connection;

    @Mock
    private PlayerAgentInterface playerAgent;

    private final long gameLength = 10000;

    @Mock
    private ThreadPoolTaskScheduler cancelFlowScheduler;

    @Mock
    private ExitServiceInteface exitService;

    private GameFlow gameFlow;

    @Mock
    MoveSubscriptionInterface subscription;

    @Before
    public void init() {
        gameFlow = spy(new GameFlow(connection, playerAgent, gameLength, cancelFlowScheduler, exitService));
        when(connection.createSubscription(any(), any(), any())).thenReturn(subscription);
    }

    @Test
    public void testGameFlow() {
        String clientSubject = (String) ReflectionTestUtils.getField(gameFlow, "clientSubject");
        String serverSubject = (String) ReflectionTestUtils.getField(gameFlow, "serverSubject");

        assertNotNull(clientSubject);
        assertNotNull(serverSubject);

        assertEquals(clientSubject.length(), 36);
        assertEquals(serverSubject.length(), 36);

        assertNotEquals(clientSubject, serverSubject);

    }

    @Test
    public void testEndFlow() throws Exception {
        //to init subscription
        gameFlow.run();
        gameFlow.endFlow();

        verify(subscription, times(1)).close();
        verify(exitService, times(1)).exit();
    }

    @Test
    public void testOnExceptionCome() throws Exception {
        gameFlow.run();
        when(playerAgent.makeFirstMove()).thenReturn(10000);
        String serverSubject = (String) ReflectionTestUtils.getField(gameFlow, "serverSubject");

        gameFlow.onExceptionCome(new StartGameException());
        verify(connection, times(1)).publishMove(serverSubject, 10000);

        gameFlow.onExceptionCome(new EndGameException());
        verify(gameFlow, times(1)).endFlow();
    }

    @Test
    public void testOnMoveCome() {
        when(playerAgent.makeMove(10000)).thenReturn(55555);
        gameFlow.onMoveCome(10000);
        String serverSubject = (String) ReflectionTestUtils.getField(gameFlow, "serverSubject");

        verify(connection, times(1)).publishMove(serverSubject, 55555);
    }

    @Captor
    ArgumentCaptor<GameConnectionAdapterInterface.MoveListener> moveListenerArgumentCaptor;
    @Captor
    ArgumentCaptor<GameConnectionAdapterInterface.ExceptionListener> exceptionListenerArgumentCaptor;

    @Test
    public void testRun() throws Exception {
        String clientSubject = (String) ReflectionTestUtils.getField(gameFlow, "clientSubject");
        GameConnectionAdapterInterface.MoveListener moveListener = mock(GameConnectionAdapterInterface.MoveListener.class);
        GameConnectionAdapterInterface.ExceptionListener exceptionListener = mock(GameConnectionAdapterInterface.ExceptionListener.class);

        gameFlow.run();
        verify(connection, times(1)).createSubscription(eq(clientSubject), moveListenerArgumentCaptor.capture(), exceptionListenerArgumentCaptor.capture());

        GameConnectionAdapterInterface.MoveListener moveLambda = moveListenerArgumentCaptor.getValue();
        GameConnectionAdapterInterface.ExceptionListener exceptionLambda = exceptionListenerArgumentCaptor.getValue();

        moveLambda.moveEvent(1000);
        verify(gameFlow).onMoveCome(1000);

        Exception e = new Exception();
        exceptionLambda.exceptionEvent(e);
        verify(gameFlow).onExceptionCome(e);
    }
}
