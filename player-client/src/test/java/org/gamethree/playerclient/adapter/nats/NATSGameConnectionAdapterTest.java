package org.gamethree.playerclient.adapter.nats;

import io.nats.client.Connection;
import io.nats.client.Dispatcher;
import io.nats.client.Message;
import io.nats.client.MessageHandler;
import org.gamethree.playerclient.adapter.EndGameException;
import org.gamethree.playerclient.adapter.GameConnectionAdapterInterface;
import org.gamethree.playerclient.adapter.StartGameException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NATSGameConnectionAdapterTest {
    private NATSGameConnectionAdapter adapter;
    private Connection nats;

    @Before
    public void init() {
        nats = mock(io.nats.client.Connection.class);
        adapter = new NATSGameConnectionAdapter(nats, "client-ready", "player", "end", "start");
        adapter = spy(adapter);
    }

    @Test
    public void testPublishClientReady() {
        adapter.publishClientReady("serverSubject", "replyToSubject");

        verify(nats, times(1)).publish("client-ready", "replyToSubject", "player@serverSubject".getBytes());
    }

    @Captor
    private ArgumentCaptor<MessageHandler> registerMessageLambdaCaptor;

    @Test
    public void testCreateSubscription() throws InterruptedException {
        GameConnectionAdapterInterface.MoveListener moveListener = mock(GameConnectionAdapterInterface.MoveListener.class);
        GameConnectionAdapterInterface.ExceptionListener exceptionListener = mock(GameConnectionAdapterInterface.ExceptionListener.class);
        Dispatcher dispatcher = mock(Dispatcher.class);
        when(nats.createDispatcher(any(MessageHandler.class))).thenReturn(dispatcher);


        adapter.createSubscription("subjectName", moveListener, exceptionListener);


        verify(nats).createDispatcher(registerMessageLambdaCaptor.capture());
        MessageHandler lambda = registerMessageLambdaCaptor.getValue();


        Message msg = mock(Message.class);
        when(msg.getData()).thenReturn("10000".getBytes());
        lambda.onMessage(msg);
        verify(adapter, times(1)).onMoveCame(msg, moveListener, exceptionListener);
    }

    @Test
    public void testOnMoveCame_end() {
        GameConnectionAdapterInterface.MoveListener moveListener = mock(GameConnectionAdapterInterface.MoveListener.class);
        GameConnectionAdapterInterface.ExceptionListener exceptionListener = mock(GameConnectionAdapterInterface.ExceptionListener.class);
        Message msg = mock(Message.class);

        //test end of the game signal
        when(msg.getData()).thenReturn("end".getBytes());
        adapter.onMoveCame(msg, moveListener, exceptionListener);

        verify(exceptionListener, times(1)).exceptionEvent(any(EndGameException.class));
    }

    @Test
    public void testOnMoveCame_start() {
        GameConnectionAdapterInterface.MoveListener moveListener = mock(GameConnectionAdapterInterface.MoveListener.class);
        GameConnectionAdapterInterface.ExceptionListener exceptionListener = mock(GameConnectionAdapterInterface.ExceptionListener.class);
        Message msg = mock(Message.class);

        //test start of the game signal
        when(msg.getData()).thenReturn("start".getBytes());
        adapter.onMoveCame(msg, moveListener, exceptionListener);

        verify(exceptionListener, times(1)).exceptionEvent(any(StartGameException.class));
    }

    @Test
    public void testOnMoveCame_number() {
        GameConnectionAdapterInterface.MoveListener moveListener = mock(GameConnectionAdapterInterface.MoveListener.class);
        GameConnectionAdapterInterface.ExceptionListener exceptionListener = mock(GameConnectionAdapterInterface.ExceptionListener.class);
        Message msg = mock(Message.class);

        //test start of the game signal
        when(msg.getData()).thenReturn("1000".getBytes());
        adapter.onMoveCame(msg, moveListener, exceptionListener);

        verify(moveListener, times(1)).moveEvent(1000);
    }
}
