package org.gamethree.playerclient.agent;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class PlayerAgentTest {

    private PlayerAgent agent;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void init() {
        agent = new PlayerAgent("Player");
    }

    @Test
    public void testMakeMove() {
        assertEquals(agent.makeMove(5), 2);
        assertEquals(agent.makeMove(6), 2);
        assertEquals(agent.makeMove(7), 2);

        assertEquals(agent.makeMove(50), 17);
        assertEquals(agent.makeMove(51), 17);
        assertEquals(agent.makeMove(52), 17);
    }

    @Test
    public void testMakeFirstMove() {
        for (int i = 0; i < 10000000; i++) {
            int move = agent.makeFirstMove();
            assertTrue(move > 1);
        }
    }
}
